# GenerationZ
Lite weight code generator for:
- [ ] Python classes

# Architecture
Class Parser:
Parses input file and converts it to Schema object.

Class Schema:
Form of class to produce.
- Constructor(\_\_init\_\_) parameters
- Variables
- Methods

Class Builder:
Builds concret string of python code.

# JOVANNI
