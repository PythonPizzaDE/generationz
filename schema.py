from dataclasses import dataclass

@dataclass
class Method:
    parameters: list[str]
    return_type = None

@dataclass
class Schema:
    constructer_parameters: list[str]
    variables: list[str]
    methods: list[Method]
    name: str
